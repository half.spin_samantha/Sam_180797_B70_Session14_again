<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 8/24/2017
 * Time: 5:55 PM
 */





    //Object

    Class MyClass{

       public $thisIsProperty;

        /**
         * @return mixed
         */
        public function getThisIsProperty()
        {
            return $this->thisIsProperty;
        }

        /**
         * @param mixed $thisIsProperty
         */
        public function setThisIsProperty($thisIsProperty)
        {
            $this->thisIsProperty = $thisIsProperty;
        }


    } // end of myclass

    $myObject1 = new MyClass();
    $myObject2 = new MyClass();

    $myObject1->setThisIsProperty(255);

    echo $myObject2->getThisIsProperty();

die;
    $myObject = new MyClass();

    var_dump($myObject);

    $myObject->setThisIsProperty(255);

    echo $myObject->getThisIsProperty();


die;
//Compound types(2)

//Associative arrays
$age = [
    34,
    36,
    "Rahim" => 23,      //Rahim,Karim are keys here
    "Karim" => 30,
    "Jorina" => 23,
    54,
    "Moynar ma" => 33,
    88
];

echo $age[3];


//Indexed arrays
$myArray = [23 , 23 , 23 ,[23 , 23 , 23 , 4 , 2 , 3], 4 , 2 , 3]; //This is multidimensional

$myArray = array(23,"Hello" , 23 , 4 , 2 , 3);







    die;
    //Scalar types(4)
    $thisIsAVariable = 123;     //Integer

    $thisIsAVariable = 12.3;    //Float

    $thisIsAVariable = true;    //Boolean

    $x = 34;

    $thisIsAVariable = "This  $x is a \"double\"  'quoted' string<br>";    //String

    echo $thisIsAVariable;

    $thisIsAVariable = 'This $x is a \'single\' "quoted" string';    //String

    echo $thisIsAVariable;

    //Heredoc.Similar to double quote
    $thisIsAVariable = <<<BASIS
    
    একই সঙ্গে সুন্দরবনের ১০ কিলোমিটার এলাকাজুড়ে কতগুলো শিল্পকারখানা রয়েছে, এর তালিকা ছয় মাসের মধ্যে আদালতে 
    দাখিল করতে বলা হয়েছে। পরিবেশসচিব, শিল্পসচিব, ভূমিসচিব ও পরিবেশ অধিদপ্তরের মহাপরিচালকের প্রতি এ নির্দেশ দেওয়া হয়েছে।

    আজ বৃহস্পতিবার বিচারপতি মইনুল ইসলাম চৌধুরী ও বিচারপতি জেবিএম হাসানের সমন্বয়ে গঠিত হাইকোর্ট 
    বেঞ্চ এক রিট আবেদনের প্রাথমিক শুনানি নিয়ে রুলসহ এ আদেশ দেন।
    
    $x

BASIS;

    echo $thisIsAVariable;

    //Nowdoc.This is similar to single quote
    $thisIsAVariable = <<<'BASIS'
    
    একই সঙ্গে সুন্দরবনের ১০ কিলোমিটার এলাকাজুড়ে কতগুলো শিল্পকারখানা রয়েছে, এর তালিকা ছয় মাসের মধ্যে আদালতে
     দাখিল করতে বলা হয়েছে। পরিবেশসচিব, শিল্পসচিব, ভূমিসচিব ও পরিবেশ অধিদপ্তরের মহাপরিচালকের প্রতি এ নির্দেশ দেওয়া হয়েছে।

    আজ বৃহস্পতিবার বিচারপতি মইনুল ইসলাম চৌধুরী ও বিচারপতি জেবিএম হাসানের সমন্বয়ে গঠিত হাইকোর্ট 
    বেঞ্চ এক রিট আবেদনের প্রাথমিক শুনানি নিয়ে রুলসহ এ আদেশ দেন।

BASIS;




